
const DEFAULT_NAME = 'Suspense';


/**
 * Error created on a suspense's waiting aborted
 */
export class AbortError extends Error {
   /**
    * @param {String} name          the name of the suspense (default: "Suspense");
    */
   constructor(name = DEFAULT_NAME) {
      super(`${name} aborted`);
   }
};

/**
 * Error created on a suspense's waiting timeout
 */
export class TimeoutError extends Error {
   /**
    * @param {String} name          the name of the suspense (default: "Suspense");
    * @param {Number|void} timeout  (optional) specify the timeout in millisecond (won't display if not a number)
    */
   constructor(name = DEFAULT_NAME, timeout) {
      super(`${name} timed out` + (timeout && !isNaN(timeout) ? ` (${timeout} ms)` : ''));
   }
}

/**
 * Default rejection error
 */
export class RejectedError extends Error {
   /**
    * @param {String} name          the name of the suspense (default: "Suspense");
    */
   constructor(name = DEFAULT_NAME) {
      super(`${name} rejected`);
   }
}

/**
 * Default rejection error
 */
export class ResetError extends Error {
   /**
    * @param {String} name          the name of the suspense (default: "Suspense");
    */
   constructor(name = DEFAULT_NAME) {
      super(`${name} reset`);
   }
}