import Suspense from '../suspense.js';


describe('Testing suspence', () => {


   it('should create suspense', () => {
      const m = new Suspense();

      expect(m.pending).toBe(true);
      expect(m.rejected).toBe(false);
      expect(m.error).toBeUndefined();
      expect(m.value).toBeUndefined();
   });

   it('should create a named suspense', () => {
      const anon = new Suspense();
      const named = new Suspense({ name: 'Test' });

      expect(anon.name).toBe('Suspense');
      expect(named.name).toBe('Test');
   });

   it('should not allow changing name', () => {
      const s = new Suspense({ name: 'TestLocked' });

      expect(() => s.name = 'Other').toThrow("Cannot assign to read only property 'name'");
      expect(s.name).toBe('TestLocked');
   });

   it('should timeout', async () => {
      const m = new Suspense({ timeout: 10 });

      await expect(m.wait()).rejects.toThrow();

      expect(m.pending).toBe(true);
   });

   it('should not timeout', async () => {
      const m = new Suspense({ timeout: 0 });

      setTimeout(() => m.reject('Test passed'), 50);

      await expect(m.wait()).rejects.toBe('Test passed');
      
      expect(m.pending).toBe(false);
      expect(m.rejected).toBe(true);
   });


   it('should prevent resolving a rejected suspense', () => {
      const m = new Suspense();
      
      m.resolve(true);

      expect(m.pending).toBe(false);
      expect(m.rejected).toBe(false);
      expect(m.value).toBe(true);
      expect(m.error).toBeUndefined();

      expect(() => m.reject()).toThrow('Invalid state');

      expect(m.pending).toBe(false);
      expect(m.rejected).toBe(false);
      expect(m.value).toBe(true);
      expect(m.error).toBeUndefined();
   });

   it('should prevent rejecting a resolved suspense', () => {
      const m = new Suspense();
      
      m.reject();

      expect(m.pending).toBe(false);
      expect(m.rejected).toBe(true);
      expect(m.value).toBeUndefined();
      expect(m.error).toBeInstanceOf(Error);

      expect(() => m.resolve(true)).toThrow('Invalid state');

      expect(m.pending).toBe(false);
      expect(m.rejected).toBe(true);
      expect(m.value).toBeUndefined();
      expect(m.error).toBeInstanceOf(Error);
   });

   it('should call `onReject` when rejected', () => {
      const error = new Error('test');
      const m = new Suspense({
         onReject: e => {
            expect(e).toBe(error);

            called = true;
         }
      });
      let called = false;

      m.reject(error);

      expect( called ).toBe(true);
   });

   it('should call `onResolve` when resolved', () => {
      const value = { foo: 'test' };
      const m = new Suspense({
         onResolve: v => {
            expect(v).toBe(value);

            called = true;
         }
      });
      let called = false;

      m.resolve(value);

      expect( called ).toBe(true);
   });

   it('should abort waiting', async () => {
      const m = new Suspense();

      await expect(m.wait({
         onWaiting: abort => abort()
      })).rejects.toThrow('Suspense aborted');

      expect(m.pending).toBe(true);
      expect(m.rejected).toBe(false);
   });

   it('should not wait on a resolved suspense', async () => {
      const m = new Suspense({ timeout: 10 });
      m.resolve();

      await expect(m.wait()).resolves.not.toThrow();
   });

   it('should not wait on a rejected suspense', async () => {
      const m = new Suspense({ timeout: 10 });
      m.reject();

      await expect(m.wait()).rejects.toThrow('Suspense rejected');
   });

   it('should wait timeout with contextual messages', async () => {
      const m = new Suspense({ name: 'Test with message', timeout: 10 });

      await expect(m.wait({ message: 'Test 1' })).rejects.toThrow('Test 1');
      await expect(m.wait({ message: 'Test 2' })).rejects.toThrow('Test 2');
      await expect(m.wait()).rejects.toThrow('Test with message timed out (10 ms)');
   });

   it('should resolve while waiting', async () => {
      const m = new Suspense({ timeout: 1000 });

      setTimeout(() => m.resolve('hello'), 10);

      expect(m.pending).toBe(true);
      expect(m.value).toBeUndefined();

      const value = await m.wait();

      expect( value ).toBe('hello');
      expect(m.pending).toBe(false);
      expect(m.rejected).toBe(false);
      expect(m.value).toBe('hello');
   });

   it('should override global wait', async () => {
      const m = new Suspense({ timeout: 1000 });

      const ts = Date.now();
      await expect( m.wait({ timeout: 10 }) ).rejects.toThrow('Suspense timed out');
      const te = Date.now();

      expect( te - ts ).toBeLessThan( 1000 );
   });

   it('should return the amount of monitors waiting', async () => {
      const m = new Suspense({ timeout: 20 });

      expect(m.waiting).toBe(0);

      const monitors = [ m.wait(), m.wait(), m.wait() ];

      expect(m.waiting).toBe(3);

      await Promise.allSettled(monitors);

      expect(m.waiting).toBe(0);
   });

   it('should reset resolved', () => {
      const m = new Suspense();
      m.resolve('test');

      expect(m.pending).toBe(false);
      expect(m.value).toBe('test');

      m.reset();

      expect(m.pending).toBe(true);
      expect(m.value).toBeUndefined();
   });

   it('should reset while waiting', async () => {
      const m = new Suspense();

      const w1 = m.wait();

      m.reset();

      return expect(w1).rejects.toThrow('Suspense reset');
   });


   describe('utilities', () => {

      it('should create a resolved suspense', () => {
         const val = 'hello';
         const m = Suspense.resolved(val);

         expect(m.pending).toBe(false);
         expect(m.rejected).toBe(false);
         expect(m.error).toBeUndefined();
         expect(m.value).toBe(val);
      });

      it('should create a rejected suspense', () => {
         const err = new Error('Test');
         const m = Suspense.rejected(err);

         expect(m.pending).toBe(false);
         expect(m.rejected).toBe(true);
         expect(m.error).toBe(err);
         expect(m.value).toBeUndefined();
      });


      describe('suspense.all', () => {

         it('should wait for all to complete', async () => {
            const list = [
               new Suspense({ name: 'm1', timeout: 100 }),
               new Suspense({ name: 'm2', timeout: 100 }),
               new Suspense({ name: 'm3', timeout: 100 }),
            ];

            setTimeout(() => list.forEach((s, i) => s.resolve(i + 1)), 1);

            await expect( Suspense.all(list) ).resolves.toEqual([ 1, 2, 3 ]);

            list.forEach((s, i) => {
               expect( s.pending ).toBe(false);
               expect( s.rejected ).toBe(false);
               expect( s.value ).toBe(i + 1);
            });
         });

         it('should reject all when one rejects', async () => {
            const list = [
               new Suspense({ name: 'm1', timeout: 100 }),
               new Suspense({ name: 'm2', timeout: 100 }),
               new Suspense({ name: 'm3', timeout: 100 }),
            ];

            setTimeout(() => list[1].reject('Test 2'));

            await expect( Suspense.all(list) ).rejects.toEqual('Test 2');

            expect( list[0].pending ).toBe(true);  // unchanged state
            expect( list[1].pending ).toBe(false);
            expect( list[1].error ).toBe('Test 2');
            expect( list[2].pending ).toBe(true);  // unchanged state

         });

      });


      describe('suspense.allSettled', () => {

         it('should wait for all to complete', async () => {
            const list = [
               new Suspense({ name: 'm1', timeout: 10 }),
               new Suspense({ name: 'm2', timeout: 10 }),
               new Suspense({ name: 'm3', timeout: 10 }),
            ];

            list[0].resolve(1);
            //list[1].reject('Test 2');
            list[2].resolve(3);
            
            // await expect( suspense.allSettled(list) ).resolves.toEqual([ 1, 2, 3 ]);

            
            await expect( Suspense.allSettled(list) ).resolves.toEqual(
               expect.arrayContaining([
                  { status: 'fulfilled', value: 1 },
                  { status: 'fulfilled', value: 3 }
               ])
            );

            expect( list[0].pending ).toBe(false);
            expect( list[0].value ).toBe(1);

            expect( list[1].pending ).toBe(true);
            expect( list[1].value ).toBeUndefined();

            expect( list[2].pending ).toBe(false);
            expect( list[2].value ).toBe(3);
            
         });

      });


      describe('suspence.any', () => {

         it('should return first resolved', async () => {
            const list = [
               new Suspense({ name: 'test1', timeout: 20 }),
               new Suspense({ name: 'test2', timeout: 10 }),
               new Suspense({ name: 'test3', timeout: 50 }),
            ];

            setTimeout(() => list[1].reject('Skipped!'));
            setTimeout(() => list[2].resolve("value3"), 30);  // make sure we wait until after list[0]

            await expect( Suspense.any(list) ).resolves.toBe('value3');
         });

         it('should reject if no value resolves', async () => {
            const list = [
               new Suspense({ name: 'test1', timeout: 20 }),
               new Suspense({ name: 'test2', timeout: 10 }),
               new Suspense({ name: 'test3', timeout: 50 }),
            ];

            await expect( Suspense.any(list) ).rejects.toThrow('All suspenses rejected');
         });

         it('should reject if no Suspense provided', async () => {
            const list = [];

            await expect( Suspense.any(list) ).rejects.toThrow('All suspenses rejected');
         });

      });


      describe('suspence.race', () => {

         it('should race suspenses (failure)', async () => {
            const list = [
               new Suspense({ name: 'test1', timeout: 20 }),
               new Suspense({ name: 'test2', timeout: 10 }),
               new Suspense({ name: 'test3', timeout: 30 }),
            ];

            await expect( Suspense.race(list) ).rejects.toThrow('test2 timed out');
         });

         it('should abort on rejected/resolved suspense', async () => {
            const list = [
               new Suspense({ name: 'm1', timeout: 100 }),
               new Suspense({ name: 'm2', timeout: 100 }),
               new Suspense({ name: 'm3', timeout: 100 }),
            ];

            list[1].reject();

            await expect( Suspense.race(list) ).rejects.toThrow('m2 rejected');
         });

         it('should abort on rejected/resolved suspense', async () => {
            const list = [
               new Suspense({ name: 'm1', timeout: 100 }),
               new Suspense({ name: 'm2', timeout: 100 }),
               new Suspense({ name: 'm3', timeout: 100 }),
            ];

            list[1].resolve('hello');

            await expect( Suspense.race(list) ).resolves.toBe('hello');
         });

      });

   });


});