import {
   AbortError,
   TimeoutError,
   RejectedError,
   ResetError
} from '../errors.js';



describe('Testing errors', () => {

   it('should all inherit from Error', () => {
      expect( AbortError.prototype ).toBeInstanceOf(Error);
      expect( TimeoutError.prototype ).toBeInstanceOf(Error);
      expect( RejectedError.prototype ).toBeInstanceOf(Error);
   });

   describe('AbortError', () => {
      
      it('should create instance without name', () => {
         const err = new AbortError();

         expect( err.message ).toBe('Suspense aborted');
      });

      it('should create instance with name', () => {
         const err = new AbortError('Test');

         expect( err.message ).toBe('Test aborted');
      });

   });

   describe('TimeoutError', () => {

      it('should create instance without name', () => {
         const err = new TimeoutError();

         expect( err.message ).toBe('Suspense timed out');
      });

      it('should create instance with timeout', () => {
         const err = new TimeoutError(void 0, 123);

         expect( err.message ).toBe('Suspense timed out (123 ms)');
      });

      it('should create instance with name', () => {
         const err = new TimeoutError('Test');

         expect( err.message ).toBe('Test timed out');
      });

      it('should create instance with timeout and name', () => {
         const err = new TimeoutError('Test', 321);

         expect( err.message ).toBe('Test timed out (321 ms)');
      });

   });


   describe('RejectionError', () => {
      
      it('should create instance without name', () => {
         const err = new RejectedError();

         expect( err.message ).toBe('Suspense rejected');
      });

      it('should create instance with name', () => {
         const err = new RejectedError('Test');

         expect( err.message ).toBe('Test rejected');
      });

   });


   describe('ResetError', () => {
      
      it('should create instance without name', () => {
         const err = new ResetError();

         expect( err.message ).toBe('Suspense reset');
      });

      it('should create instance with name', () => {
         const err = new ResetError('Test');

         expect( err.message ).toBe('Test reset');
      });

   });

});