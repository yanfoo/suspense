import { AbortError, TimeoutError, RejectedError, ResetError } from './errors.js';


const DEFAULT_NAME = 'Suspense';
const DEFAULT_TIMEOUT = 3000;
const INIT_PENDING = true;
const INIT_VALUE = undefined;
const INIT_ERROR = undefined;

const $internal = Symbol('$internal');

const _initInternal = (s, { name, timeout, onResolve, onReject }) => {
   const resolvers = new Set();

   Object.defineProperties(s, {
      name: {
         configurable: false,
         enumerable: true,
         writable: false,
         value: name
      },
      timeout: {
         configurable: false,
         enumerable: true,
         writable: false,
         value: timeout
      },
      [$internal]: {
         configurable: false,
         enumerable: false,
         writable: false,
         value: {
            pending: INIT_PENDING,
            value: INIT_VALUE,
            error: INIT_ERROR,
            onResolve,
            onReject,
            resolvers,
         }
      }
   });
};

const _getInternal = (s, property) => s[$internal][property];

const _wait = (s, { timeout, onWaiting, message }) => s[$internal].pending ? new Promise((resolve, reject) => {
   const resolver = (error = s[$internal].error) => {
      if (!timer) return;   // if there is no timer, then the promise has already been resolved or rejected

      if (timer !== true) {
         clearTimeout(timer);
      }
      timer = null;  // no more timer, indicating that this context should no longer wait

      s[$internal].resolvers.delete(resolver);
      
      if (error) {
         reject(error);
      } else {
         resolve(s[$internal].value);
      }
   };
   timeout = isNaN(timeout) ? s.timeout : timeout;

   let timer = timeout > 0 ? setTimeout(() => resolver(new TimeoutError(message || s.name, timeout)), timeout) : true;

   s[$internal].resolvers.add(resolver);

   if (onWaiting) {
      onWaiting(error => resolver(error || new AbortError(message || s.name)));
   }
}) : s[$internal].error ? Promise.reject(s[$internal].error) : Promise.resolve(s[$internal].value);

const _resolve = (s, value, error) => {
   if (!s[$internal].pending) {
      throw new Error('Invalid state');
   }

   s[$internal].pending = false;

   if (error) {
      s[$internal].error = error;
   } else {
      s[$internal].value = value;
   }

   // cleanup then resolve everything...
   for (const resolver of s[$internal].resolvers) {
      resolver(error);
   }

   s[$internal].resolvers.clear();
   
   if (error) {
      s[$internal].onReject && s[$internal].onReject(error);
   } else {
      s[$internal].onResolve && s[$internal].onResolve(value);
   }

   return value;
};

const _reset = (s) => {
   if (s[$internal].pending) {
      return _resolve(s, undefined, new ResetError(s.name));
   } else {
      s[$internal].pending = INIT_PENDING;
      s[$internal].value = INIT_VALUE;
      s[$internal].error = INIT_ERROR;
   }
};


class Suspense {

   constructor(options) {
      const {
         name = DEFAULT_NAME,
         timeout = DEFAULT_TIMEOUT,
         onResolve,
         onReject,
      } = options || {};

      _initInternal(this, { name, timeout, onResolve, onReject });
   }

   get pending() {
      return _getInternal(this, 'pending');
   }

   get rejected() {
      return !!_getInternal(this, 'error');
   }

   get error() {
      return _getInternal(this, 'error');
   }

   get value() {
      return _getInternal(this, 'value');
   }

   get waiting() {
      return _getInternal(this, 'resolvers').size;
   }

   async wait(options) {
      return _wait(this, options || {});
   }

   resolve(value) {
      return _resolve(this, value);
   }

   reject(error) {
      _resolve(this, undefined, error || new RejectedError(this.name));
   }

   reset() {
      _reset(this);
   }

}


Suspense.resolved = value => {
   const rs = new Suspense();
   rs.resolve(value);
   return rs;
};

Suspense.rejected = error => {
   const rs = new Suspense();
   rs.reject(error);
   return rs;

};


Suspense.all = async arr => {
   const cleanup = [];
   return Promise.all(arr.map(value => value.wait({
      onWaiting: abort => cleanup.push(abort)
   }).catch(error => {
      cleanup.forEach(abort => abort());
      throw error;
   })));
};

Suspense.allSettled = async arr => Promise.allSettled(arr.map(value => value.wait()));

Suspense.any = async arr => {
   const cleanup = [];
   let hasResolved = false;

   return new Promise(resolve => {
      let pending = arr.length;

      if (pending) {
         arr.forEach(value => value.wait({
            onWaiting: abort => cleanup.push(abort)
         }).then(value => {
            hasResolved = true;
            resolve(value);
         }).catch(() => {
            pending = pending - 1;
            if (!pending) {
               resolve();
            }
         }));
      } else {
         resolve();
      }
   }).then(value => {
      if (hasResolved) {
         cleanup.forEach(abort => abort());
         return value;
      } else {
         throw new RejectedError('All suspenses');
      }
   });
};

Suspense.race = async arr => {
   const cleanup = [];

   return Promise.race(arr.map(value => value.wait({
      onWaiting: abort => cleanup.push(abort)
   }))).finally(() => {
      cleanup.forEach(abort => abort());
   });
};


export default Suspense;
export { AbortError, TimeoutError, RejectedError, ResetError };