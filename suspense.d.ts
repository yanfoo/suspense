
declare const DEFAULT_NAME:string = 'Suspense';
declare const DEFAULT_TIMEOUT:number = 3000;

/**
 * The Suspense constructor options
 */
declare interface SuspenseOptions<Type> {
   /**
    * The name of the Suspense being created
    */
   name?:string = DEFAULT_NAME;
   /**
    * The default timeout of the waiting period
    */
   timeout?:number = DEFAULT_TIMEOUT;
   /**
    * The resolve event handler, a function receiving the resolved value.
    */
   onResolve?:(value:Type) => void;
   /**
    * The reject event handler, a function receiving the rejected error.
    */
   onReject?:(error:Error) => void;
};

/**
 * The Suspense wait method options overrides
 */
declare interface SuspenseWaitOptions {
   /**
    * Override the Suspense's default timeout waiting period
    */
   timeout?:number;
   /**
    * Callback passing an abort function for the current waiting period.
    * The abort function can receive an error that will be used to reject
    * the waiting Promise.
    * 
    * Note: calling the abort will only abort the current waiting period,
    *       it will not cancel other waiting periods, neither affect the
    *       state of the current Suspense.
    */
   onWaiting?:(abort:(error?:Error|any) => void) => void;
   /**
    * The message to set the AbortError or TimeoutError errors, defaults
    * to the Suspense's name.
    */
   message?:string
}

/**
 * A class providing methods to asynchronously wait and set values for
 * data synchronization.
 */
declare class Suspense<Type> {

   /**
    * Returns a resolved Suspense
    * @param value the value of the resolved Suspense
    */
   static resolved<Type>(value:Type):Suspense<Type>;

   /**
    * Returns a rejected Suspense
    * @param error an optional error, defaults to a RejectedError
    */
   static rejected(error?:Error):Suspense<void>;

   /**
    * Wait until all Suspenses are resolved. Will return a rejected
    * Suepsne if at least one Suspense is rejected. This method is
    * similar to Promise.all.
    * @param arr an array of Suspense to wait for
    */
   static all(arr:[Suspense<any>]):Promise<[any]>;

   /**
    * Wait for all Suspenses to be either resolved or rejected, and
    * returns their values. This method is similar to Promise.allSettled.
    * @param arr an array of Suspense to wait for
    */
   static allSettled(arr:[Suspense<any>]):Promise<[any]>;

   /**
    * Wait until a Suspense resolves, ignoring rejected ones, then abort
    * any other ones waiting. This method is similar to Promise.any.
    * @param arr an array of Suspense to wait for
    */
   static any(arr:[Suspense<any>]):Promise<any>;

   /**
    * Wait for the first Suspense that either rejects or resolves, then
    * aobrt any other waiting. This method is similar to Promise.reace.
    * @param arr an array of Suspense to wait for
    */
   static race(arr:[Suspense<any>]):Promise<any>;


   /**
    * The name of the Suepense
    */
   name:string = DEFAULT_NAME;

   /**
    * The timeout of this Suspense
    */
   readonly timeout:number = DEFAULT_TIMEOUT;

   /**
    * Create a new Suspense instance
    * @param options the optional configuration options
    */
   constructor(options?:SuspenseOptions<Type>);

   /**
    * Returns true if the suspense is neither resolved or rejected
    */
   get pending():bool;

   /**
    * Returns true if the suspense was rejected
    */
   get rejected():bool;

   /**
    * If the suspense is rejected, return the error, otherwise returns undefined.
    */
   get error():Error|any|void;

   /**
    * Synchronous get the Suspense's value. May be undefined if pending is true.
    */
   get value():Type|void;

   /**
    * Return the number of calls are waiting on this Suspense.
    */
   get waiting():number

   /**
    * Wait for this Suspense. If the Suspense was rejected, returns a rejected Promise
    * containing the error, if any. If the suspense was resolved, returns a resolved
    * Promise with the Suspense's value.
    * @param options the waiting options
    */
   wait(options:SuspenseWaitOptions):Promise<V>;

   /**
    * Resolve the Suepnse with the specified value, if provided. The method will return
    * the argument value.
    * @param value the value to resolve with
    */
   resolve(value?:Type):Type

   /**
    * Reject the Suspense with a specified error, if provided, otherwise it will default
    * to a RejectedError.
    * @param error the error, or RejectedError if not provided
    */
   reject(error?:Error):void

   /**
    * If the Suspense has been either resolved or rejected, it will be reset as if it
    * had just been created. If the Suspense is still pending, all waiting handles
    * will be rejected with a ResetError
    */
   reset():void
}

/**
 * This error is generated when the waiting period of a Suspense is aborted.
 */
declare class AbortError extends Error {
   /**
    * Create a new AbortError
    * @param name the Suepnse's name
    */
   constructor(name?:string = DEFAULT_NAME);
}

/**
 * This error is generated when the Suspense was rejected while waiting.
 */
declare class RejectedError extends Error {
   /**
    * Create a new RejectedError
    * @param name the Suspense's name
    */
   constructor(name:string = DEFAULT_NAME);
}

/**
 * This error is generated when the Suspense has been reset while waiting
 */
 declare class ResetdError extends Error {
   /**
    * Create a new ResetdError
    * @param name the Suspense's name
    */
   constructor(name:string = DEFAULT_NAME);
}

/**
 * This error is generated when the waiting period has timed out.
 */
declare class TimeoutError extends Error {
   /**
    * Create a new TimeoutError
    * @param name the Suepnse's name
    * @param timeout the optional timeout that was waited
    */
   constructor(name:string = DEFAULT_NAME, timeout?:number);
}



export default Suspense;
export {
   AbortError,
   RejectedError,
   ResetdError,
   TimeoutError,

   SuspenseOptions,
   SuspenseWaitOptions
};